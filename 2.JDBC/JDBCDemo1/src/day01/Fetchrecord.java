package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Fetchrecord {
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter Employee ID:");
		int empId = scanner.nextInt();

		String selectQuery = "SELECT * FROM employee WHERE empId = " + empId;

		try {
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(selectQuery);

			if (resultSet.next()) {
				int id = resultSet.getInt("empId");
				String empName = resultSet.getString("empName");
				double salary = resultSet.getDouble("salary");
				String gender = resultSet.getString("gender");
				String emailId = resultSet.getString("emailId");
				String password = resultSet.getString("password");

				System.out.println("Employee ID: " + id);
				System.out.println("Employee Name: " + empName);
				System.out.println("Salary: " + salary);
				System.out.println("Gender: " + gender);
				System.out.println("Email ID: " + emailId);
				System.out.println("Password: " + password);
			} else {
				System.out.println("Employee with ID " + empId + " not found.");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					statement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		scanner.close();
	}
}
