package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner; 

import com.db.DbConnection;

public class InsertRecord{
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		Scanner scanner = new Scanner(System.in); 

		System.out.println("Enter Employee ID:");
		int empId = scanner.nextInt(); 

		System.out.println("Enter Employee Name:");
		String empName = scanner.next(); 

		System.out.println("Enter Salary:");
		double salary = scanner.nextDouble(); 

		System.out.println("Enter Gender:");
		String gender = scanner.next(); 

		System.out.println("Enter Email ID:");
		String emailId = scanner.next(); 

		System.out.println("Enter Password:");
		String password = scanner.next(); 
		String insertQuery = "insert into employee values (" + 
				empId + ", '" + empName + "', " + salary + ", '" + 
				gender + "', '" + emailId + "', '" + password + "')";

		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);

			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					statement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


		scanner.close();
	}
}
