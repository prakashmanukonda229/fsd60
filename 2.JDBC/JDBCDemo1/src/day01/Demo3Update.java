package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import com.db.DbConnection;


///Update Utkarsh Record and update salary to 25000
public class Demo3Update {
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;

		String empName = "Utkarsh";
		double newSalary = 25000.00;

		String updateQuery = "update employee set salary = " + newSalary + " where empName = '" + empName + "'";

		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateQuery);

			if (result > 0) {
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Record Update Failed...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

